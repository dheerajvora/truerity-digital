const swiper = new Swiper('.swiper-container', {
    loop: true,
    slidesPerView: 1,
    navigation: {
      nextEl: '.swiper-button-n',
      prevEl: '.swiper-button-p',
    }
  });
 
  $('#service-slider').owlCarousel({
    loop:false,
    margin:10,
    dots:false,
    nav:true,
    items:1
})
  $(document).ready(function() {
    $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
  
      fixedContentPos: false
    });
  });
  $('#pricing').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        800:{
            items:2
        },
        1000:{
            items:3
        }
    }
});
$('#testimonial').owlCarousel({
  loop:false,
  margin:10,
  nav:true,
  dots:false,
  responsive:{
      0:{
          items:1
      },
      800:{
          items:2
      },
      1000:{
          items:2
      }
  }
});
$(window).scroll(function() {    
  var scroll = $(window).scrollTop();
  if (scroll >= 100) {
      $("#mynav").addClass("bg-primary");
  }
  else{
    $("#mynav").removeClass("bg-primary");
  }
});